Composable Models and Views [![Build Status](https://travis-ci.org/mpourismaiel/Composable-Front-End.svg?branch=master)](https://travis-ci.org/mpourismaiel/Composable-Front-End)
-----------------------
The idea here is to make a MV framework that is composable, meaning you can
make any kind of model or view you want from pre-written parts or use presets.

## Installation:
Just get a clone and run `npm run compile`.

*Right now you can only use the views*:
- view (Just displays template)
- modeledView (Compiles the template with data)
- collectionView (Generates view according to a collection of data)

*And models*:
- model (A basic model)
- collectionModel (A collection of models)

## Future features
- Syncing models
- Listeners

## Changelog
#### Jan 15 #8f1cbc64
- Tests are passing and I'm happy!
