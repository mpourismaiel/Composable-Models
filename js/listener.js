export let listener = {
  events: {},

  trigger (event) {
    if (typeof this.events[event] === 'undefined') return
    this.events[event].map((fn) => fn.call())
  },

  on (event, callback) {
    if (this.events[event] !== undefined) {
      this.events[event].push(callback)
    } else {
      this.events[event] = [callback]
    }
  },

  off (event) {
    if (this.events[event] !== undefined) {
      delete this.events[event]
    }
  }
}

export let Listener = () => {
  return Object.assign({}, listener)
}
