import { Listener } from './listener'

export let modelBasic = {
  attributes: {},
  changed: false,

  set (options, value = '') {
    if (typeof options === 'object') {
      Object.assign(this.attributes, options)
    } else {
      let key = options
      this.attributes[key] = value
    }

    this.changed = true
  },

  get (key) {
    return this.attributes[key]
  }
}

export let modelExtraFunctions = {
  has (key) {
    if (typeof this.get(key) !== 'undefined') return true
    return false
  },

  unset (key) {
    if (this.has(key)) delete this.attributes[key]
  },

  keys () {
    return Object.keys(this.attributes)
  }
}

export let collectionBasic = {
  models: [],
  model: null,
  length: 0,

  // Generate a new model and insert the given data as attributes and put it
  // inside collection
  add (attributes) {
    let model = this.model(attributes)
    this.models[this.models.length] = model

    this.length = this.models.length
    return this.models.length - 1
  },

  // Get the model using it's id
  get (id) {
    if (typeof id === 'symbol') {
      return this.models.filter((model) => model.id === id)[0]
    } else if (typeof id === 'number') {
      return this.models[id]
    }
  },

  // Remove a model from the collection given an specific index
  remove (index) {
    this.models.splice(index, 1)
  },
}

export let collectionExtraFunctions = {
  pop () {
    return this.models.pop()
  },

  shift () {
    return this.models.shift()
  }
}

export let model = (options = {}) => {
  let model = Object.assign({}, modelBasic, {
    set (options = {}) {
      modelBasic.set.call(this, options)
      for (let key of Object.keys(options)) {
        this.trigger(`${key}:change`)
      }
    }
  }, modelExtraFunctions, Listener())

  model.attributes = options
  model.id = Symbol('model')
  model.set(options)
  model.changed = false
  return model
}

export let collectionModel = (options) => {
  return Object.assign({}, collectionBasic, collectionExtraFunctions, Listener, options)
}
