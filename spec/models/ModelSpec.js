describe('A model', () => {
  var modelAdvanced = require('../../build/js/models').model
  var model, KEY, VALUE

  beforeEach(() => {
    KEY = 'KEY'
    VALUE = 'VALUE'
    model = modelAdvanced({[KEY]: VALUE})
  })

  it('should be able to set a value', () => {
    expect(model.get(KEY)).toEqual(VALUE)
  })

  it('should be able to return attribute keys', () => {
    expect(model.keys()).toEqual([KEY])
  })

  describe('The attributes variable', () => {
    it('should be able to set a value', () => {
      var ANOTHER_VALUE = Symbol('another value')
      model.set({[KEY]: ANOTHER_VALUE})
      expect(model.get(KEY)).toEqual(ANOTHER_VALUE)
    })

    it('should be able to check whether a value exists', () => {
      expect(model.has(KEY)).toEqual(true)
    })

    it('should be able to remove an attribute', () => {
      model.unset(KEY)
      expect(model.has(KEY)).toEqual(false)
    })
  })
})

describe('A collection', () => {
  var collectionAdvanced = require('../../build/js/models').collectionModel
  var modelAdvanced = require('../../build/js/models').model
  var collection, KEY, VALUE, id

  beforeEach(() => {
    KEY = 'KEY'
    VALUE = 'VALUE'
    collection = collectionAdvanced({model: modelAdvanced})
  })

  describe('with basic functionalities', () => {
    it('should be able to add new items and get it by key', () => {
      id = collection.add({
        [KEY]: VALUE
      })
      expect(collection.get(id).get(KEY)).toEqual(VALUE)
    })

    it ('should be able to remove the key', () => {
      collection.remove(id)
      expect(collection.length).toEqual(0)
    })
  })

  describe('with all functionalities', () => {
    beforeEach(() => {
      collection.add({
        [KEY]: VALUE
      })
      collection.add({
        [KEY]: VALUE + '2'
      })
      collection.add({
        [KEY]: VALUE + '3'
      })
    })

    it('should be able to pop an item', () => {
      expect(collection.pop().get(KEY)).toEqual(VALUE + '3')
    })

    it('should be able to shift an item', () => {
      expect(collection.shift().get(KEY)).toEqual(VALUE)
    })
  })
})
