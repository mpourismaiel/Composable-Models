module.exports = {
  entry: [
    './js/models.js',
    './js/views.js',
    './js/listener.js',
  ],
  output: {
    filename: './build/[name].js'
  },
  devtool: 'source-map',
  resolveLoader: {
    modulesDirectories: [
      'node_packages',
      'web_loaders',
      'web_modules',
      'node_loaders',
      'node_modules'
    ]
  },
  stats: {
    colors: true,
    reasons: true
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      },{
        test: /\.json$/,
        loader: 'json'
      }
    ]
  }
}
